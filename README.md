# gradle-plugin-kotlin


[[_TOC_]]


# Usage
The gradle-plugin-kotlin template is implemented utilizing 
[cookiecutter](https://github.com/cookiecutter/cookiecutter). 

cookiecutter [installation](https://cookiecutter.readthedocs.io/en/latest/installation.html) and  
[usage](https://cookiecutter.readthedocs.io/en/stable/README.html#for-users-of-existing-templates) instructions can be 
found on the cookiecutter [documentation site](https://readthedocs.org/projects/cookiecutter/). 

To create a new project using this template execute the following command:

```shell
cookiecutter https://gitlab.com/federal-aviation-administration/ang-c55/templates/gradle-plugin-kotlin
``` 

cookiecutter will prompt for several project attributes and then generate a project into the current working directory.


# License

## Creative Commons Zero v1.0 Universal
This is a work created by or on behalf of the United States Government. To the extent that this work is not already in
the public domain by virtue of 17 USC § 105, the FAA waives copyright and neighboring rights in the work worldwide
through the CC0 1.0 Universal Public Domain Dedication (which can be found at https://creativecommons.org/publicdomain/zero/1.0/).
See LICENSE.txt and NOTICE.txt in the root of this project for the full terms of the license.
