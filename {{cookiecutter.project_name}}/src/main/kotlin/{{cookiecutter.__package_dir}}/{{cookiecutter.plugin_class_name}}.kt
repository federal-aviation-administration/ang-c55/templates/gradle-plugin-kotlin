/*
 * {{cookiecutter.project_name}}
 *
 * This is a work created by or on behalf of the United States Government. To the
 * extent that this work is not already in the public domain by virtue of
 * 17 USC § 105, the FAA waives copyright and neighboring rights in the work
 * worldwide through the CC0 1.0 Universal Public Domain Dedication (which can be
 * found at https://creativecommons.org/publicdomain/zero/1.0/).
 */
@file:Suppress("UnstableApiUsage")

package {{cookiecutter.package}}

import org.gradle.api.Project
import org.gradle.api.Plugin
import org.gradle.api.tasks.testing.Test

/**
 * A Gradle plugin to assist in providing opinionated/conventional configuration of test tasks.
 *
 * @author US DOT, FAA, Office of NextGen, Modeling and Simulation Branch
 */
class {{cookiecutter.plugin_class_name}}: Plugin<Project> {

    /**
     * Applies the plugin to the provided project.
     *
     * @param theProject the Project to which the plugin should be applied
     */
    override fun apply(theProject: Project) {

        theProject.logger.debug("Applying {{cookiecutter.project_name}} plugin.")

        // Plugin Stuff Here

        theProject.logger.debug("{{cookiecutter.project_name}} plugin applied!")

    }
}
