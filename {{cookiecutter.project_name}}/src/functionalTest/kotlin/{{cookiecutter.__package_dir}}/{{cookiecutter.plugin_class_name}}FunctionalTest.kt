/*
 * {{cookiecutter.project_name}}
 *
 * This is a work created by or on behalf of the United States Government. To the
 * extent that this work is not already in the public domain by virtue of
 * 17 USC § 105, the FAA waives copyright and neighboring rights in the work
 * worldwide through the CC0 1.0 Universal Public Domain Dedication (which can be
 * found at https://creativecommons.org/publicdomain/zero/1.0/).
 */

package {{cookiecutter.package}}

import io.kotlintest.specs.BehaviorSpec
import java.io.File
import org.gradle.testkit.runner.GradleRunner

/**
 * @author US DOT, FAA, Office of NextGen, Modeling and Simulation Branch
 */
class {{cookiecutter.plugin_class_name}}FunctionalTest: BehaviorSpec({

    Given("An empty project") {

        // Setup the test build
        val projectDir = File("build/functionalTest")
        projectDir.mkdirs()

        And("a build config with only the plugin specified") {

            projectDir.resolve("settings.gradle").writeText("")
            projectDir.resolve("build.gradle").writeText(
                    """
                        plugins {
                            id('{{cookiecutter.plugin_id}}')
                        }
                    """
            )

            When("executing the build with no arguments") {

                val runner = GradleRunner.create()
                runner.withProjectDir(projectDir)
                runner.forwardOutput()
                runner.withPluginClasspath()
                val result = runner.build()

                Then("the build should be successful"){
                    result.output.contains("BUILD SUCCESSFUL")
                }
            }
        }
    }
})
