/*
 * {{cookiecutter.project_name}}
 *
 * This is a work created by or on behalf of the United States Government. To the
 * extent that this work is not already in the public domain by virtue of
 * 17 USC § 105, the FAA waives copyright and neighboring rights in the work
 * worldwide through the CC0 1.0 Universal Public Domain Dedication (which can be
 * found at https://creativecommons.org/publicdomain/zero/1.0/).
 */

package {{cookiecutter.package}}

import io.kotlintest.shouldBe
import io.kotlintest.shouldNotBe
import io.kotlintest.specs.BehaviorSpec
import org.gradle.api.internal.tasks.testing.junitplatform.JUnitPlatformTestFramework
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.tasks.testing.Test
import org.gradle.testfixtures.ProjectBuilder

/**
 * @author US DOT, FAA, Office of NextGen, Modeling and Simulation Branch
 */
class {{cookiecutter.plugin_class_name}}IntegrationTest : BehaviorSpec({

    Given("a project with default values") {

        val project = ProjectBuilder.builder().build()

        When ("applying the plugin") {

            project.pluginManager.apply("{{cookiecutter.plugin_id}}")

            Then("the plugin is succesfully applied to the project") {

                project.plugins.getPlugin({{cookiecutter.plugin_class_name}}::class.java) shouldNotBe null

            }
        }
    }
})
